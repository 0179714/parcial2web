users
 - username
 - password
 - first_name
 - last_name
 - date_created
 - upid
/*create table users(
	username VARCHAR(45) NOT NULL, 
	password VARCHAR(45) NOT NULL,
	first_name VARCHAR(450),
	last_name VARCHAR(450),
	region VARCHAR(450),
	email VARCHAR(450),
	date_created DATETIME,
	upid VARCHAR(45) NOT NULL
) DEFAULT CHARACTER SET=utf8;
messages
 - text
 - from 
 - to
 - date_created
 - date_seen
 - media
create table messages(
	message_text TEXT NOT NULL, 
	message_read TINYINT(1) NOT NULL, 
	user_from VARCHAR(45) NOT NULL, 
	user_to VARCHAR(45) NOT NULL, 
	date_created DATETIME NOT NULL, 
	date_seen DATETIME, 
	media VARCHAR(450)
) DEFAULT CHARACTER SET=utf8;

profile
 - upid
 - profile_picture
 - banner
 - status
 - address
create table profile(
	upid VARCHAR(45) NOT NULL, 
	profile_picture VARCHAR(450), 
	banner VARCHAR(140), 
	status VARCHAR(140), 
	address VARCHAR(450)
) DEFAULT CHARACTER SET=utf8;
*/