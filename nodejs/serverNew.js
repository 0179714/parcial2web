// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var db = require("./mysql_local");
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var sessions = require("client-sessions");


//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);


//Cookie
//===================================================================================
app.use(sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object 
  secret: 'blargadeeblargblarg', // should be a large unguessable string 
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms 
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds 
}));




var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();               


router.route('/logIn').post(function(req, res){

	var mysql_conn = db.connection();
	var query = "select * from users where password = MD5('"+req.body.password+"') and username = '"+req.body.username+"';"
	console.log(query);

	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log(rows.length);
	  
	  if(rows.length == 1){
	  	req.session.user = rows[0];
	  	res.json({ message: "SUCCESS" });
	  }
	  else{
	  	res.json({ message: "ERROR" });
	  }

	});


	mysql_conn.end(); 
});

router.route('/Who').get(function(req, res){
	res.json("El usuario es: " + req.session.user);
});

// accessed at GET http://localhost:port/getUsers
router.route('/getUsers').get(function(req, res) {
	var mysql_conn = db.connection();
  	var dateFormat = require('dateformat');	
  	var formattedDate = dateFormat(Date.parse(req.query.date), "yyyy-mm-dd h:MM:ss");
	var query = "select * from users where date_created > '" + formattedDate + "' and region = '" + req.query.region + "';";
		
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log(rows);
	  
      res.json({ result: rows, message: 'Statement executed: ' + query, response: "SUCCESS" });
	});

	mysql_conn.end();
    
});

// accessed at POST http://localhost:port/registerUser
router.route('/registerUser').post(function(req, res) {
	var mysql_conn = db.connection();
	var query = "insert into users(username, first_name, last_name, email, date_created, region, password) values('"+
		req.body.username+"', '"+req.body.firstname+"', '"+req.body.lastname+"', '"+req.body.email+"', NOW(), '"+req.body.region+
		"', MD5('"+req.body.password+"'));";
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Row inserted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/deleteUsers
router.route('/deleteUsers').delete(function(req, res) {
	var mysql_conn = db.connection();
	var query = "truncate table users;"
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Table users reseted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});


// accessed at POST http://localhost:port/deleteRowUsers
router.route('/deleteRowUsers').delete(function(req, res) {
	var mysql_conn = db.connection();
	var query = "delete from users where username = '"+req.body.username+"';"
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Row from users deleted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});


// accessed at POST http://localhost:port/sendMessage
router.route('/sendMessage').post(function(req, res) {
	var mysql_conn = db.connection();
	var query = "insert into messages(message_text, message_read, user_from, user_to, date_created) values('"+ 
		req.body.message+"', 0, '"+req.body.user_from+"', '"+req.body.user_to+"', NOW());";
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Row inserted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});


// accessed at GET http://localhost:port/getMessages
router.route('/getMessages').get(function(req, res) {
	var mysql_conn = db.connection();
	var conn2 = db.connection();
	var queryGet = "select * from messages where message_read = 0 and (user_to = '" + req.query.user_to + "' or user_from = '" + req.query.user_from + "');";
	var queryUpdate = "update messages set message_read = 1, date_seen = NOW() where user_to = '" + req.query.user_to + "' or user_from = '" + req.query.user_from + "';";
	var query = queryUpdate + queryGet;

	mysql_conn.connect();
	mysql_conn.query(queryUpdate, function(err, rows, fields) {
	  if (err) throw err;
	  console.log(queryUpdate);
	  console.log(rows);

	  	conn2.connect();
		conn2.query(queryGet, function(err, rows, fields) {
			if (err) throw err;
			console.log(queryUpdate);
			console.log(rows);
			conn2.end();
			mysql_conn.end();
			res.json({ result: rows, message: 'Statement(s) executed: ' + query, response: "SUCCESS" });		 		 
		});


	});
});

app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Starting server on port ' + port);