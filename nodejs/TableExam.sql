create table users2(
	username VARCHAR(45) NOT NULL, 
	password VARCHAR(45) NOT NULL,
	first_name VARCHAR(450),
	last_name VARCHAR(450),
	region VARCHAR(450),
	email VARCHAR(450),
	date_created DATETIME,
	upid VARCHAR(45) NOT NULL
) DEFAULT CHARACTER SET=utf8;